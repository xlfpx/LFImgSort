#include "StdAfx.h"
#include "welcome_screen.h"
#include "utility.h"

void welcome_screen() {

	cls();
	cout << endl;
	print_centeredln("- - - Welcome to the LF-Image-Sorter! - - -");
	waiting_animation(2000);
	cout << endl << endl << endl;

}