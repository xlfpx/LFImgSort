#pragma once

void init();
void pick_folder();
void analyze_folder();
void errHandleFolderInvalid(char* reason);
void setupExiv();
void create_folders();
void move_photos();
void copy_photos();
void exciv_photos();
void setupExiv();
void final_rename();
void read_new_names();