#include "StdAfx.h"
#include "utility.h"

// global variables
char header_carray[81];

void set_header_text(char* left_header, char* right_header){

	// local variables
	char space[80];

	 // calculating space in between left and right headers
	ZeroMemory(space, 80);
	FillMemory(space, (80 - strlen(left_header) - strlen(right_header)), ' ');

	// fill into global header-variable
	ZeroMemory(header_carray, 80); // clear old header
	strcpy_s(header_carray, left_header);
	strcat_s(header_carray, space);
	strcat_s(header_carray, right_header);

}

void resetHeaderText() {

	set_header_text(STANDAD_LEFT_HEADER, STANDAD_RIGHT_HEADER);

}

void cls() {

	cout << header_carray << endl;

}

void print_centered(char* what_to_print) {

	char space[80];
	// calculating space in between left and right headers
	ZeroMemory(space, 80);
	FillMemory(space, (40 - (int)(strlen(what_to_print) * 0.5f)), ' ');

	cout << space << what_to_print;

}

void print_centeredln(char* what_to_print) {

	print_centered(what_to_print);
	cout << endl;

}

void waiting_animation(float milliseconds) {

	Sleep(milliseconds); // TODO make this a nice loading bar with counting number in the middle

}

void showConsoleCursor(bool showFlag) { // credit: http://stackoverflow.com/users/845568/captain-obvlious

	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;
	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);

}

COORD getConsoleCursorPosition() {

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	if (!GetConsoleScreenBufferInfo(
		GetStdHandle(STD_OUTPUT_HANDLE),
		&csbi
		))
		DebugBreak();
	
	return csbi.dwCursorPosition;

}

bool ProcessRunning(const char* name)
{
	HANDLE SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (SnapShot == INVALID_HANDLE_VALUE)
		return false;

	PROCESSENTRY32 procEntry;
	procEntry.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(SnapShot, &procEntry))
		return false;

	do
	{
		if (strcmp(procEntry.szExeFile, name) == 0)
			return true;
	} while (Process32Next(SnapShot, &procEntry));

	return false;
}

void reset_line_cursor() {

	COORD newPosition;
	newPosition.X = 0;
	newPosition.Y = getConsoleCursorPosition().Y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), newPosition);

}

void rewrite_line(char* new_line_content) {

	reset_line_cursor();
	cout << new_line_content;

}

void rewrite_clear_line(char* new_line_content) {

	char nnlc[81];
	char buffer[81];
	ZeroMemory(buffer, 81);
	strcpy(nnlc, new_line_content);
	if (strlen(nnlc) < 80) {
	
		FillMemory(buffer, 79 - strlen(nnlc), ' ');
		strcat(nnlc, buffer);

	}
	rewrite_line(nnlc);

}

void rewrite_line(wchar_t* new_line_content) {

	reset_line_cursor();
	cout << wcsToCarray(new_line_content);

}

void rewrite_clear_line(wchar_t* new_line_content) {

	wchar_t nnlc[81];
	wchar_t buffer[81];
	ZeroMemory(buffer, sizeof(wchar_t) * 81);
	wcscpy(nnlc, new_line_content);
	if (wcslen(nnlc) < 80) {

		wmemset(buffer, L' ', 79 - wcslen(nnlc));
		wcscat(nnlc, buffer);

	}
	rewrite_line(nnlc);

}

void uul(char* text) {

	char line[80];
	strcpy(line, "     ");
	strcat(line, text);
	rewrite_clear_line(line);

}

void add_log(char* log) {

	// TODO add log-file
	COORD newPosition;
	newPosition.X = 0;
	newPosition.Y = getConsoleCursorPosition().Y - 2;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), newPosition);

	rewrite_clear_line(" "); 
	reset_line_cursor();
	cout << " - " << log << endl;
	rewrite_clear_line(" ");
	cout << endl;
	rewrite_clear_line(" "); 
	cout << endl;

}

char* wcsToCarray(wchar_t* input) {

	static char mybuffer[256];
	wcstombs(mybuffer, input, 256);
	return mybuffer;

}

wchar_t* BrowseFolder(__out wchar_t* folderPath)
{
	wchar_t path[MAX_PATH];

	BROWSEINFOW bi = { 0 };
	bi.lpszTitle = L"Browse for folder...";
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;

	LPITEMIDLIST pidl = SHBrowseForFolderW(&bi);

	if (pidl != 0)
	{
		//get the name of the folder and put it in path
		SHGetPathFromIDListW(pidl, path);

		//free memory used
		IMalloc * imalloc = 0;
		if (SUCCEEDED(SHGetMalloc(&imalloc)))
		{
			imalloc->Free(pidl);
			imalloc->Release();
		}

		wcscpy(folderPath, path);
		wcscat(folderPath, L"\\");
		return folderPath;
	}

	wcscpy(folderPath, L"ERROR");
	return folderPath;

}

BYTE* loadRCIM(LPCSTR ID, LPCSTR RT, DWORD* pDwSize) {

	BYTE* pointer_to_data;

	HMODULE hModule = GetModuleHandle(NULL); // get the handle to the current module (the executable file)
	HRSRC hResource = FindResource(hModule, ID, RT);
	HGLOBAL hMemory = LoadResource(hModule, hResource);
	int dwSize = (int)SizeofResource(hModule, hResource);
	(*pDwSize) = dwSize;

	LPVOID lpAddress = LockResource(hMemory);
	pointer_to_data = (BYTE*) malloc(dwSize);
	memcpy(pointer_to_data, lpAddress, dwSize);
	UnlockResource(hMemory);

	return pointer_to_data;

}

void writeFIM(void* pointer_to_file_in_memory, int size, char* path) {

	if (pointer_to_file_in_memory != nullptr) {

		ofstream writefim;
		writefim.open(path, ios::out | ios::binary);

		writefim.write((char*)pointer_to_file_in_memory, size);

		writefim.close();

	}
	else {

		OutputDebugStringA("MEMORYMAN_writeFIM called with invalid pointer!");

	}

}

wchar_t* makeProgBar(float progress, int length, wchar_t bound, wchar_t fill, wchar_t empty, bool display_perc)
{
	static wchar_t bar[81];
	ZeroMemory(bar, 81);

	if (length > 75)
		DebugBreak();

	bar[0] = bound;
	for (int i_for = 0; i_for < length; i_for ++) {
		
		if(((float)i_for / (float)length) < progress)
			bar[i_for+1] = fill;
		else
			bar[i_for+1] = empty;

		bar[i_for + 2] = bound;
		bar[i_for + 3] = NULL;
	}

	if (display_perc) {

		wchar_t numbuffer[8];
		_itow((int)(progress * 100.0f), numbuffer, 10);
		wcscat(bar, L" ");
		wcscat(bar, numbuffer);
		wcscat(bar, L"%");

	}

	return bar;

}
