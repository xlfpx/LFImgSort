#include "StdAfx.h"
#include "main.h"
#include "welcome_screen.h"
#include "utility.h"
#include "resource.h"

using namespace std;

wchar_t folderPath[MAX_PATH];
wchar_t exiv2_path_buffer[MAX_PATH];
wchar_t exivdll_path_buffer[MAX_PATH];
vector<string> names;
int element_count = 1;

int main() {

	init();
	welcome_screen();
	pick_folder();
	analyze_folder();
	create_folders();
	move_photos();
	copy_photos();
	setupExiv();
	exciv_photos();
	read_new_names();
	final_rename();

	getchar();
	return 0;

}

void init() {

	showConsoleCursor(false);
	resetHeaderText();

}

void analyze_folder() {

	wchar_t analyze_folder_line[81];
	uul(wcsToCarray(wcscpy(analyze_folder_line, L"analyzing folder ...")));
	Sleep(1000);
	wchar_t folderBufferPath[MAX_PATH];
	wcscpy(folderBufferPath, folderPath);
	wcscat(folderBufferPath, L"*");
	HANDLE hfindhandle = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATAW hFind;
	wchar_t elementcountbuffer[16];
	for (hfindhandle = FindFirstFileW(folderBufferPath, &hFind); hfindhandle != INVALID_HANDLE_VALUE; ) {
	
		_itow(element_count, elementcountbuffer, 10);
		wcscpy(analyze_folder_line, L"analyzing folder - found ");
		wcscat(analyze_folder_line, elementcountbuffer);
		wcscat(analyze_folder_line, L" elements");
		uul(wcsToCarray(analyze_folder_line)); // TODO make this rewrite only the last few characters which are new
		
		if (hFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY/* && hFind.cFileName[0] != '.'*/) {
			element_count--; // doesn't count
		}else if(hFind.cFileName[0] == L'.'){
			element_count--; // doesn't count
		}else if (	
					(			(hFind.cFileName[wcslen(hFind.cFileName) - 4] == L'.')
					&&	(tolower(hFind.cFileName[wcslen(hFind.cFileName) - 3]) == L'j')
					&&	(tolower(hFind.cFileName[wcslen(hFind.cFileName) - 2]) == L'p')
					&&	(tolower(hFind.cFileName[wcslen(hFind.cFileName) - 1]) == L'g')) == false
				&& ((hFind.cFileName[wcslen(hFind.cFileName) - 4] == L'.')
					&& (tolower(hFind.cFileName[wcslen(hFind.cFileName) - 3]) == L'p')
					&& (tolower(hFind.cFileName[wcslen(hFind.cFileName) - 2]) == L'n')
					&& (tolower(hFind.cFileName[wcslen(hFind.cFileName) - 1]) == L'g')) == false){

			wchar_t error_buffer[256];
			wcscpy(error_buffer, L"the following file is not in one of the supported formats (*.jpg, *.png): ");
			wcscat(error_buffer, hFind.cFileName);
			errHandleFolderInvalid(wcsToCarray(error_buffer));
			return;

		}

		if (FindNextFileW(hfindhandle, &hFind) == 0)
			break;
		element_count ++;
	}
	wcscpy(analyze_folder_line, L"found ");
	wcscat(analyze_folder_line, elementcountbuffer);
	wcscat(analyze_folder_line, L" elements");
	add_log(wcsToCarray(analyze_folder_line));
	Sleep(1000);
	if(hfindhandle == INVALID_HANDLE_VALUE){
		errHandleFolderInvalid("the folder is empty.");
		return;
	}

	FindClose(hfindhandle);

}

void create_folders() {

	uul("creating folders ... ");

	wchar_t orig_folder_path [MAX_PATH];
	wchar_t rnm_folder_path[MAX_PATH];
	wcscpy(orig_folder_path, folderPath);
	wcscat(orig_folder_path, L"originals");
	wcscpy(rnm_folder_path, folderPath);
	wcscat(rnm_folder_path, L"renamed");
	_wmkdir(orig_folder_path);
	_wmkdir(rnm_folder_path);
	add_log("created folders");

}

void move_photos()
{

	uul("moving files ... ");

	wchar_t photoPath1[MAX_PATH];
	wchar_t photoPath2[MAX_PATH];
	HANDLE hfindhandle = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATAW hFind;
	wchar_t folderBufferPath[MAX_PATH];
	wcscpy(folderBufferPath, folderPath);
	wcscat(folderBufferPath, L"*");
	for (hfindhandle = FindFirstFileW(folderBufferPath, &hFind); hfindhandle != INVALID_HANDLE_VALUE; ) {

		if (!(hFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)/* && hFind.cFileName[0] != '.'*/) {
			wcscpy(photoPath1, folderPath);
			wcscat(photoPath1, hFind.cFileName);
			wcscpy(photoPath2, folderPath);
			wcscat(photoPath2, L"originals\\");
			wcscat(photoPath2, hFind.cFileName);
			MoveFileW(photoPath1, photoPath2);
		}
		
		if (FindNextFileW(hfindhandle, &hFind) == 0)
			break;

	}
	FindClose(hfindhandle);
	add_log("moved files");
	

}

void copy_photos()
{

	wchar_t copy_photos_line[80];
	uul(wcsToCarray(wcscpy(copy_photos_line, L"copying files ... ")));

	wchar_t photoPath1[MAX_PATH];
	wchar_t photoPath2[MAX_PATH];
	HANDLE hfindhandle = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATAW hFind;
	wchar_t folderBufferPath[MAX_PATH];
	wcscpy(folderBufferPath, folderPath);
	wcscat(folderBufferPath, L"originals\\*");
	int copy_count = 1;
	for (hfindhandle = FindFirstFileW(folderBufferPath, &hFind); hfindhandle != INVALID_HANDLE_VALUE; ) {

		wcscpy(photoPath1, folderPath);
		wcscat(photoPath1, L"originals\\");
		wcscat(photoPath1, hFind.cFileName);
		wcscpy(photoPath2, folderPath);
		wcscat(photoPath2, L"renamed\\");
		wcscat(photoPath2, hFind.cFileName);
		CopyFileW(photoPath1, photoPath2, false);

		wcscpy(copy_photos_line, L"copying files ... ");
		wcscat(copy_photos_line, makeProgBar((float)((float)copy_count / (float)element_count), 40, L'#', '=', '-', true));
		uul(wcsToCarray(copy_photos_line));

		if (FindNextFileW(hfindhandle, &hFind) == 0)
			break;
		copy_count ++;
	}
	FindClose(hfindhandle);
	add_log("copied files");

}

void exciv_photos()
{

	uul("running exiv ... ");
	wchar_t exivCommand[128];
	wcscpy(exivCommand, L"-F rename \"");
	wcscat(exivCommand, folderPath);
	wcscat(exivCommand, L"renamed\\*\"");
	ShellExecuteW(NULL, nullptr, exiv2_path_buffer, exivCommand, folderPath, SW_HIDE);

	while(ProcessRunning("exiv2.exe")){ // while waiting for exiv, play animation

		if ((int)((float)timeGetTime() / 300.0f) % 8 == 0) {
		
			uul("running exiv  ");
		
		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 1) {

			uul("running exiv  .");

		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 2) {

			uul("running exiv  ..");

		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 3) {

			uul("running exiv  ...");

		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 4) {

			uul("running exiv  ....");

		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 5) {

			uul("running exiv   ...");

		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 6) {

			uul("running exiv    ..");

		}else if ((int)((float)timeGetTime() / 300.0f) % 8 == 7) {

			uul("running exiv     .");

		}

		Sleep(50);

	}
	add_log("exiv complete");

}

void errHandleFolderInvalid(char* reason) {

	char statement[256];
	strcpy(statement, "     Error! The process cannot be continued because ");
	strcat(statement, reason);
	cout << endl << statement;

}

void pick_folder() {

	char pick_folder_line[80];
	strcpy(pick_folder_line, "-> please pick the folder containing the photos you want to rename!");
	uul(pick_folder_line);
	Sleep(800);
	
	strcpy(pick_folder_line, "-?-> couldn't get any path... maybe try again?");
	while (wcscmp(BrowseFolder(folderPath), L"ERROR") == 0) {
	
		uul(pick_folder_line);
		if(strlen(pick_folder_line) + 1 < 79)
			strcat(pick_folder_line, "?"); // add a '?' everytime the folder-choosing-dialog fails
		Sleep(500);

	}
	
	strcpy(pick_folder_line, "successfully set path to \""); // TODO make add to list function for this including logfile to be deleted on successful finish
	if (78 - strlen(pick_folder_line) < wcslen(folderPath)) {
		folderPath[78 - strlen(pick_folder_line) - 3]
			= folderPath[78 - strlen(pick_folder_line) - 2]
				= folderPath[78 - strlen(pick_folder_line) - 1]
					= '.';
		
		folderPath[78 - strlen(pick_folder_line)] = NULL;
	}
	char pathbuffer[MAX_PATH];
	wcstombs(pathbuffer, folderPath, MAX_PATH);
	strcat(pick_folder_line, pathbuffer);
	strcat(pick_folder_line, "\"");
	add_log(pick_folder_line);
	Sleep(1000);

}

void setupExiv() {

	uul("preparing exciv ...");

	wcscpy(exiv2_path_buffer, folderPath);
	wcscat(exiv2_path_buffer, L"exiv2.exe");
	// MessageBoxW(NULL, exiv2_path_buffer, L"exivpath", MB_OK); // todo add to log-file only
	GetTempPathW(MAX_PATH, exivdll_path_buffer);
	wcscpy(exivdll_path_buffer, folderPath);
	wcscat(exivdll_path_buffer, L"libexpat.dll");
	DWORD exiv_exe_data_size, exiv_dll_data_size;
	uul("preparing exciv ... loading exe");
	BYTE* exiv_exe_data = loadRCIM(MAKEINTRESOURCE(IDR_EXECUTABLE1), RT_RCDATA, &exiv_exe_data_size);
	uul("preparing exciv ... loading dll");
	BYTE* exiv_dll_data = loadRCIM(MAKEINTRESOURCE(IDR_DLL1), RT_RCDATA, &exiv_dll_data_size);
	uul("preparing exciv ... writing exe");
	writeFIM(exiv_exe_data, exiv_exe_data_size, wcsToCarray(exiv2_path_buffer));
	uul("preparing exciv ... writing dll");
	writeFIM(exiv_dll_data, exiv_dll_data_size, wcsToCarray(exivdll_path_buffer));

	add_log("exciv ready");

}

void final_rename()
{
	wchar_t final_rename_line[81];
	uul("renaming final files ... ");

	for (int i_for = 0; i_for < names.size(); i_for++) {

		char path1[MAX_PATH];
		char path2[MAX_PATH];
		char numBuf[8];
	
		strcpy(path1, wcsToCarray(folderPath));
		strcat(path1, "renamed\\");
		strcpy(path2, path1);
		strcat(path1, names[i_for].c_str());
		_itoa(i_for + 1, numBuf, 10);
		strcat(path2, numBuf);
		strcat(path2, PathFindExtension(path1));
		rename(path1, path2);

		wcscpy(final_rename_line, L"reading new file-names ... ");
		wcscat(final_rename_line, makeProgBar((float)i_for / (float)names.size(), 40, L'|', L'=', L'-', true));
		uul(wcsToCarray(final_rename_line)); // TODO make this rewrite only the last few characters which are new


	}

	add_log("files renamed");

}

void read_new_names()
{

	wchar_t read_new_names_line[81];
	uul(wcsToCarray(wcscpy(read_new_names_line, L"reading new file-names ...")));
	Sleep(1000);
	wchar_t folderBufferPath[MAX_PATH];
	wcscpy(folderBufferPath, folderPath);
	wcscat(folderBufferPath, L"\\renamed\\*");
	HANDLE hfindhandle = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATAW hFind;
	int mynewcount = 1;
	for (hfindhandle = FindFirstFileW(folderBufferPath, &hFind); hfindhandle != INVALID_HANDLE_VALUE; ) {

		if (hFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY/* && hFind.cFileName[0] != '.'*/) {
			element_count --; // doesn't count
		}
		else {
		
			string new_name = wcsToCarray(hFind.cFileName);
			names.push_back(new_name);
		
		}

		wcscpy(read_new_names_line, L"reading new file-names ... ");
		wcscat(read_new_names_line, makeProgBar((float)mynewcount / (float)element_count, 40, L'|', L'=', L'-', true));
		uul(wcsToCarray(read_new_names_line)); // TODO make this rewrite only the last few characters which are new

		if (FindNextFileW(hfindhandle, &hFind) == 0)
			break;
		mynewcount ++;
	}
	FindClose(hfindhandle);

	sort(names.begin(), names.end());

	add_log("file-names sorted");

}
