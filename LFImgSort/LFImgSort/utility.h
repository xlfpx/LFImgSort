#pragma once
// settings
#define STANDAD_LEFT_HEADER ""
#define STANDAD_RIGHT_HEADER "coded in April 2016 by Lars Fr\224lich"

// functions -----------------
// header
void resetHeaderText();
void set_header_text(char* left_header, char* right_header);
void waiting_animation(float milliseconds);

// helpers
void print_centered(char* what_to_print);
void print_centeredln(char* what_to_print); // with endl
void reset_line_cursor();
void rewrite_line(char* new_line_content);
void rewrite_line(wchar_t* new_line_content);
void rewrite_clear_line(char* new_line_content);
void rewrite_clear_line(wchar_t* new_line_content);
wchar_t * makeProgBar(float progress, int length, wchar_t bound, wchar_t fill, wchar_t empty, bool display_perc);
void showConsoleCursor(bool showFlag);
BYTE* loadRCIM(LPCSTR ID, LPCSTR RT, DWORD* pDwSize);
void writeFIM(void* pointer_to_file_in_memory, int size, char* path);
char* wcsToCarray(wchar_t* input);
wchar_t* BrowseFolder(__out wchar_t* folderPath);
COORD getConsoleCursorPosition();
bool ProcessRunning(const char* name);

// small integrity
void cls();
void uul(char* text);
void add_log(char* log);
